/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.EmptyStackException;
import java.util.Stack;


/**
 * 
 * ALGORISME:
 * 
 * 1. While there are still tokens to be read in,
   1.1 Get the next token.
   1.2 If the token is:
       1.2.1 A number: push it onto the value stack.
       1.2.2 A variable: get its value, and push onto the value stack.
       1.2.3 A left parenthesis: push it onto the operator stack.
       1.2.4 A right parenthesis:
         1 While the thing on top of the operator stack is not a 
           left parenthesis,
             1 Pop the operator from the operator stack.  
             2 Pop the value stack twice, getting two operands.
             3 Apply the operator to the operands, in the correct order.
             4 Push the result onto the value stack.
         2 Pop the left parenthesis from the operator stack, and discard it.
       1.2.5 An operator (call it thisOp):
         1 While the operator stack is not empty, and the top thing on the
           operator stack has the same or greater precedence as thisOp,
           1 Pop the operator from the operator stack.
           2 Pop the value stack twice, getting two operands.
           3 Apply the operator to the operands, in the correct order.
           4 Push the result onto the value stack.
         2 Push thisOp onto the operator stack.
2. While the operator stack is not empty,
    1 Pop the operator from the operator stack.
    2 Pop the value stack twice, getting two operands.
    3 Apply the operator to the operands, in the correct order.
    4 Push the result onto the value stack.
3. At this point the operator stack should be empty, and the value
   stack should have only one value in it, which is the final result.
  
 * @author Manel Orós  Adaptat de: https://www.geeksforgeeks.org/expression-evaluation/
 * 
 * Darrera rev. 10/23
 */


/***
 * Calculadora que retorna un resultat (sempre en base 10)  a partir d'una expressió matemàtica en 
 * qualsevol base.
 * Accepta decimals
 * Accepta parèntesis
 * Accepta operadors
 * Accepta diferents bases de numeració
 * 
 * @author manel
 */
public class Calculadora
{
	public static Double avalua(String expression, int base) throws CalculadoraException
	{
                //verifiquen si la base està admesa
                if (! Tools.verificaBase(base))
                    throw new CalculadoraException("La base no està permesa: " + base);

                // eliminem espais anteriors i posteriors
                expression = expression.trim();
                
                //verifiquem si comença per signe + o - i li afegim un zero al principi
                if (expression.charAt(0) == '+' || expression.charAt(0) == '-')
                    expression = "0" + expression;
                
		//array de caracters a tractar
                char[] tokens = expression.toCharArray();

		// Pila per a valors
		Stack<Double> values = new Stack<>();

		// Pila d'operadors
		Stack<Character> ops = new Stack<>();
                
                // indica el darrer caracter analitzat
                TipusCaracter caracterAnteriorAnalitzat = TipusCaracter.INICI;
                
                // recorrem l'expressió un a un, començant per l'esquerra
		for (int i = 0; i < tokens.length; i++)
		{
                    // Si és un espai, l'ignorem i avancem una posició
                    if (tokens[i] == ' ') i++;

                    // Si és un nombre, llavors el tractem
                    if (Tools.esNombre(Character.toString(tokens[i]), base))
                    {
                            StringBuilder sbuf = new StringBuilder();

                            // Construcció del dígit complet, que pot estar format per més d'un numero 
                            // o amb caràcters decimals
                            while (i < tokens.length && ( Tools.esNombre(Character.toString(tokens[i]), base) || tokens[i] == '.')) sbuf.append(tokens[i++]);
                            
                            //conversió de la base d'entrada a base 10 per a seguir amb els càlculs
                            // si és base diferent de 10, llavors no s'admet part fraccionaria
                            try
                            {
                                if (base == 10)
                                    values.push(Double.parseDouble(sbuf.toString()));
                                else
                                {
                                    Integer num = Integer.parseInt(sbuf.toString(), base);
                                    values.push(Double.valueOf(num));
                                }
                            }catch (NumberFormatException ex)
                            {
                                throw new CalculadoraException("Format incorrecte: " + sbuf.toString());
                            }
                            
                            // En aquest punt el punter està desplaçat un caràcter més a la dreta del que toca. El fem retrocedir.
                            i--;
                            
                            caracterAnteriorAnalitzat = TipusCaracter.NOMBRE;
                    }

                    // si és operador admès
                    else if (Tools.esOperador(Character.toString(tokens[i])))
                    {
                        if (caracterAnteriorAnalitzat == TipusCaracter.OPERADOR)
                            throw new CalculadoraException("Dos operadors seguits no permesos: " + i);
                        
                        if (caracterAnteriorAnalitzat == TipusCaracter.INICI)
                            throw new CalculadoraException("Signe a l'inici no permès: " + i);
                                
                        // mentre l'operador anterior a processar (de la pila) tingui més prioritat (precedència) que el nou operador a inserir
                        // i no trobem un parèntesis a la pila
                        while (!ops.empty() && esOperadorPrecedent(tokens[i], ops.peek()) && (ops.peek() != ')') && (ops.peek() != '('))
                            // treiem un operador i dos valors, calculem i posem resultat a la pila d'operadors
                            values.push(opera(ops.pop(), values.pop(), values.pop()));

                        // en aquest punt ja podem inserir el nou operador
                        ops.push(tokens[i]);   

                        caracterAnteriorAnalitzat = TipusCaracter.OPERADOR;
                    }
                    
                    else if ( (tokens[i] == '(' && caracterAnteriorAnalitzat == TipusCaracter.NOMBRE)  || (tokens[i] == ')' && caracterAnteriorAnalitzat == TipusCaracter.OPERADOR) ) 
                            throw new CalculadoraException("Parentesi obert desprès d'un nombre o tancat desprès d'un operador no permès: " + i);
                    
                    else if (tokens[i] == '(')
                    {   
                        // si és un parentesis obert, l'afegim a la pila d'operacions    
                        ops.push(tokens[i]);
                    
                        caracterAnteriorAnalitzat = TipusCaracter.PARENTESIS;
                    }

                    // si es tanca parentesis, llavors resolem tot cap enrrere (esquerra) fins la obertura
                    else if (tokens[i] == ')')
                    {   
                        // anem recorrent operadors de la pila fins arribar al tancament
                        while (ops.peek() != '(')
                            values.push(opera(ops.pop(), values.pop(), values.pop()));

                        // treiem el parentesi
                        ops.pop();

                        caracterAnteriorAnalitzat = TipusCaracter.PARENTESIS;
                    }
                    else
                        throw new CalculadoraException("Caracter no vàlid: " + tokens[i]);
		}

		// en aquest punt hem recorregut i simplificat tota l'expressió sencera, sense parèntesis, per tant, treiem un operador i dos valors, calculem i posem resultat a la pila d'operadors

		while (!ops.empty())
                    try
                    {
			values.push(opera(ops.pop(), values.pop(), values.pop()));
                    }
                        catch (EmptyStackException ex)
                    {
                        throw new CalculadoraException("Numero d'operands o operadors incorrecte");
                    }

		// al final, la pila de valors ha quedat reduïda al resultat
		return values.pop();
	}
        
        /***
         * 
         * @param op1
         * @param op2
         * @return true si op2 te la mateixa o més precedencia que op1. false si op1 té més precedència que op2
         */
        
	private static boolean esOperadorPrecedent(char op1, char op2)
	{
            Boolean ret = true;
            
            //els operadors ja estan ordenats per precedencia
            String operadors = "+-*/^"; 
            
            if (operadors.indexOf(op1) ==4)
                ret = false;
            else if (operadors.indexOf(op1) >=2 && operadors.indexOf(op2) < 2)
                ret = false;
            
            return ret;
        }

	/***
         * Realitza l'operació i retorna el resultat
         * @param op + - * /
         * @param b 
         * @param a
         * @return 
         */
	private static Double opera(char op,Double b, Double a) throws CalculadoraException
	{
            Double ret = 0D;
            
		switch (op)
		{
		case '+': ret = a + b; break;
		case '-': ret =  a - b; break;
		case '*': ret = a * b; break;
		case '/': if (b == 0)
                             throw new DivisioPerZeroException("Divisó entre zero");
                          else
                             ret =  a / b; break;
                case '^': ret = Math.pow(a, b);
                }
                
		return ret;
	}
}

