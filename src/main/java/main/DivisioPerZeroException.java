/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author manel
 */
public class DivisioPerZeroException extends CalculadoraException {

    public DivisioPerZeroException() {
    }

    public DivisioPerZeroException(String message) {
        super(message);
    }

    public DivisioPerZeroException(String message, Throwable cause) {
        super(message, cause);
    }

    public DivisioPerZeroException(Throwable cause) {
        super(cause);
    }

    public DivisioPerZeroException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
