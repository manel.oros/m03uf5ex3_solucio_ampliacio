/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author manel
 */
public class Tools {
    
    
    /***
     * Verifica si un string conté únicament els caracters de la base especificada
     * @param s
     * @param base (2,10,16)
     * @return 
     */
    public static boolean esNombre(String s, int base)
    {
        boolean ret;
        
        String exprBase10 = ("^[0-9]+$");
        String exprBase2 = ("^[0-1]+$");
        String exprBase16 = ("^[0-9A-Fa-f]+$");
        String regles;
        
        switch (base)
        {
            case 2: regles = exprBase2; break;
            case 10: regles = exprBase10; break;
            case 16: regles = exprBase16; break;    
            default: regles ="";
        }
        
        // fa match únicament si l'expressió està composada per 
        // una seqüència de dígits de principi a fí, sense cap interrupció per
        // quelsevol altre caràcter
        ret = s.matches(regles);
                
        return ret;
    }
    
      /***
     * Verifica si un string conté unicament els operadors admesos
     * @param s
     * @return 
     */
    public static boolean esOperador(String s)
    {
        boolean ret;
        
        // patró que fa match únicament si l'expressió està composada per 
        // una seqüència de dígits de principi a fí, sense cap interrupció per
        // quelsevol altre caràcter
        String regles = "^[+-/*^]+$";
        
        ret = s.matches(regles);
                
        return ret;
    }
    
    public static boolean verificaBase(int b)
    {
        boolean ret = true;
        
        List<Integer> admeses= new ArrayList<>(Arrays.asList(10,2,16));
        
        if (!admeses.contains(b))
            ret = false;
        
        return ret;
    }
    
}
